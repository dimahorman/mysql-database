create table enterprise
(
    title varchar(45) not null
        primary key
);

create table person
(
    name varchar(45) not null
        primary key,
    age decimal(45) null,
    city varchar(45) null,
    license_number decimal null
);

create table car
(
    number int not null
        primary key,
    model varchar(45) null,
    trade_mark varchar(45) null,
    driver_name varchar(45) not null,
    constraint fk_car_driver1
        foreign key (driver_name) references person (name)
);

create index fk_car_driver1_idx
    on car (driver_name);

create table trade_mark
(
    id int auto_increment
        primary key,
    title varchar(45) null,
    owner varchar(45) null
);

create table parking_garage
(
    id int auto_increment
        primary key,
    name varchar(45) null,
    address varchar(45) null,
    occupancy decimal null,
    place_quantity decimal null,
    trade_mark_id int not null,
    constraint fk_parking_garage_trade_mark1
        foreign key (trade_mark_id) references trade_mark (id)
);

create index fk_parking_garage_trade_mark1_idx
    on parking_garage (trade_mark_id);

create table parking_place
(
    id int auto_increment,
    parking_garage_id int not null,
    primary key (id, parking_garage_id),
    constraint fk_parking_place_parking_garage1
        foreign key (parking_garage_id) references parking_garage (id)
);

create table client_card
(
    id int auto_increment
        primary key,
    end_time datetime null,
    start_time datetime null,
    person_name varchar(45) not null,
    price double null,
    parking_place_id int not null,
    parking_garage_id int not null,
    constraint fk_client_card_parking_place1
        foreign key (parking_place_id, parking_garage_id) references parking_place (id, parking_garage_id),
    constraint fk_client_card_person1
        foreign key (person_name) references person (name)
);

create index fk_client_card_parking_place1_idx
    on client_card (parking_place_id, parking_garage_id);

create index fk_client_card_person1_idx
    on client_card (person_name);

create table coupon
(
    id int auto_increment
        primary key,
    car_number int not null,
    start_time time null,
    end_time time null,
    parking_place_id int not null,
    parking_garage_id int not null,
    price double null,
    constraint fk_coupon_car1
        foreign key (car_number) references car (number),
    constraint fk_coupon_parking_place1
        foreign key (parking_place_id, parking_garage_id) references parking_place (id, parking_garage_id)
);

create index fk_coupon_car1_idx
    on coupon (car_number);

create index fk_coupon_parking_place1_idx
    on coupon (parking_place_id, parking_garage_id);

create table enterprise_card
(
    parking_place_id int auto_increment,
    parking_garage_id int not null,
    Enterprise_title varchar(45) not null,
    id int not null
        primary key,
    start_time datetime null,
    end_time datetime null,
    price double null,
    constraint fk_enterprise_card_Enterprise1
        foreign key (Enterprise_title) references enterprise (title),
    constraint fk_enterprise_card_parking_place1
        foreign key (parking_place_id, parking_garage_id) references parking_place (id, parking_garage_id)
);

create index fk_enterprise_card_Enterprise1_idx
    on enterprise_card (Enterprise_title);

create index fk_enterprise_card_parking_place1_idx
    on enterprise_card (parking_place_id, parking_garage_id);

create index fk_parking_place_parking_garage1_idx
    on parking_place (parking_garage_id);

create table reservation
(
    id int auto_increment
        primary key,
    car_number int not null,
    person_name varchar(45) not null,
    start_time datetime null,
    end_time datetime null,
    parking_place_id int not null,
    parking_garage_id int not null,
    price double null,
    constraint fk_reservation_car1
        foreign key (car_number) references car (number),
    constraint fk_reservation_parking_place1
        foreign key (parking_place_id, parking_garage_id) references parking_place (id, parking_garage_id),
    constraint fk_reservation_person1
        foreign key (person_name) references person (name)
);

create index fk_reservation_car1_idx
    on reservation (car_number);

create index fk_reservation_parking_place1_idx
    on reservation (parking_place_id, parking_garage_id);

create index fk_reservation_person1_idx
    on reservation (person_name);

create definer = postgres@localhost procedure build_table()
begin
declare nothing_in_the_rows INTEGER default 0;
declare cursor1 cursor for select * from student;
declare continue handler for not found set nothing_in_the_rows = 1;
open cursor1;

all_alerts_loop:
    LOOP
        if nothing_in_the_rows != 1 then
create table if not exists city
(
    id   int AUTO_INCREMENT PRIMARY KEY,
    name varchar(45)
) ENGINE = INNODB;
leave all_alerts_loop;
end if;

end loop;
close cursor1;
end;

create definer = postgres@localhost procedure build_table1()
begin
declare nothing_in_the_rows INTEGER default 0;
declare cursor1 cursor for select * from student_has_arrear;
declare continue handler for not found set nothing_in_the_rows = 1;
open cursor1;

all_alerts_loop:
    LOOP
        if nothing_in_the_rows != 1 then
create table if not exists city
(
    id   int AUTO_INCREMENT PRIMARY KEY,
    name varchar(45),
    population INT
) ENGINE = INNODB;
leave all_alerts_loop;
end if;

end loop;
close cursor1;
end;

create definer = postgres@localhost function getAverageRating() returns int
begin
declare averageRating integer;
set averageRating = (select rating from mydb.student);
return averageRating;
end;

create definer = postgres@localhost function getAverageRating1() returns int
begin
declare averageRating integer;
set averageRating =  avg(mydb.student.rating);
return averageRating;
end;

create definer = postgres@localhost function getAverageRating2() returns int
begin
declare averageRating integer;
set averageRating = (select avg(rating) from mydb.student);
return averageRating;
end;

create definer = postgres@localhost function getGroupName(id_param int) returns varchar(45)
begin
declare groupName VARCHAR(45);

set groupName = (select name
                 from mydb.`group`
                         where id in (select group_id from student where id = id_param));
return groupName;

end;

create definer = postgres@localhost procedure many_to_many()
begin
declare c_student_id integer;
declare c_arrear_id integer;
declare cursor1 cursor for select id from mydb.student;
declare cursor2 cursor for select id from mydb.arrear;
open cursor1;
open cursor2;
FETCH cursor1 INTO c_student_id;
FETCH cursor2 INTO c_arrear_id;
insert into mydb.student_has_arrear (student_id, arrear_id) values (c_student_id, c_arrear_id);
close cursor1;
close cursor2;
end;

create definer = postgres@localhost procedure parametric_insert(IN title_param varchar(45))
begin
insert into mydb.secondary_education(title) values (title_param) ;
end;

